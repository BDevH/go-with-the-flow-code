package handlers

import (
	"github.com/labstack/echo/v4"

	"hartree.stfc.ac.uk/hbaas-server/context"
)

type APIMessage struct {
	Message string `json:"message"`
}

func NewAPIMessage(message string) APIMessage {
	return APIMessage{Message: message}
}

func RegisterHandlers(g *echo.Group, appContext context.Context) {
	birthdayHandler := BirthdayHandler{Context: appContext}
	birthdayHandler.registerEndpoints(g)

	maintenanceHandler := MaintenanceHandler{}
	maintenanceHandler.registerEndpoints(g)
}
